Altium Designer Pick and Place Locations
C:\Users\winuser\Desktop\REPEATER REDESIGN\PCB Design\Repeater_Redesign\Project Outputs for Repeater_Redesign\Pick Place for Repeater_Redesign_ASCII.txt

========================================================================================================================
File Design Information:

Date:       20-08-21
Time:       09:50
Revision:   Not in VersionControl
Variant:    No variations
Units used: mil

Designator Comment                 Layer       Footprint                 Center-X(mil) Center-Y(mil) Rotation Description                            
MH4        MH                      TopLayer    MH_3MM                    3469.000      128.000       0        ""                                     
MH3        MH                      TopLayer    MH_3MM                    128.000       129.000       0        ""                                     
MH2        MH                      TopLayer    MH_3MM                    3473.000      1843.000      0        ""                                     
MH1        MH                      TopLayer    MH_3MM                    129.000       1842.000      0        ""                                     
FD2        TP                      BottomLayer FIDUCIAL                  337.000       152.000       270      ""                                     
FD1        TP                      TopLayer    Fiducial                  317.000       1887.000      90       ""                                     
TP10       TP                      BottomLayer FIDUCIAL                  1897.000      1434.000      270      ""                                     
TP9        TP                      BottomLayer FIDUCIAL                  1897.000      1734.000      180      ""                                     
TP8        TP                      BottomLayer FIDUCIAL                  2697.000      934.000       360      ""                                     
TP7        TP                      BottomLayer FIDUCIAL                  2497.000      934.000       360      ""                                     
TP6        TP                      BottomLayer FIDUCIAL                  2297.000      934.000       360      ""                                     
TP5        TP                      BottomLayer FIDUCIAL                  2297.000      434.000       180      ""                                     
TP4        TP                      BottomLayer FIDUCIAL                  2697.000      434.000       180      ""                                     
TP3        TP                      BottomLayer FIDUCIAL                  2897.000      1834.000      180      ""                                     
TP2        TP                      BottomLayer FIDUCIAL                  400.000       400.000       180      ""                                     
TP1        TP                      BottomLayer FIDUCIAL                  389.000       650.000       180      ""                                     
VR1        B72214S0351K101         TopLayer    VARISTOR_B72214S_14_TDK   557.362       1165.693      360      ""                                     
U1         LS03-13B03R3            TopLayer    LS03-13BxxR3              1029.000      700.201       360      ""                                     
R5         ESR03EZPF1001           TopLayer    J1-0603                   1717.000      1598.000      270      Resistor                               
R4         WR06X3300FTL            TopLayer    J1-0603                   1897.000      319.000       90       Resistor                               
R3         WR06X3300FTL            TopLayer    J1-0603                   2157.000      319.000       90       Resistor                               
R2         RS02B12R00FE70          TopLayer    RS02B12R00FE70            114.000       1306.197      90       Resistor                               
R1         ESR03EZPF1001           TopLayer    J1-0603                   2932.000      664.000       90       Resistor                               
P5         "PROGRAMMING CONNECTOR" TopLayer    TC2030-MCP-NL             3227.000      189.000       270      ""                                     
P4         "Header 4X1H"           TopLayer    HDR1X4                    2662.000      114.000       360      "Header, 4-Pin"                        
P3         "Header 6X2H"           TopLayer    6X2HEADER                 2535.008      666.008       360      "Header, 6-Pin, Dual row, Right Angle" 
P2         "Header 6X2H"           TopLayer    6X2HEADER                 2535.000      1673.016      360      "Header, 6-Pin, Dual row, Right Angle" 
P1         Vin_85-305VAC           TopLayer    2pin_Screw_conn_5mm_pitch 213.000       476.575       270      "Header, 2-Pin"                        
L4         BLM18PG471SN1D          TopLayer    IND_BLM18_0603_MUR-M      2408.500      214.000       360      Inductor                               
L3         BLM18PG471SN1D          TopLayer    IND_BLM18_0603_MUR-M      3238.000      571.000       180      Inductor                               
L2         BWVS005050204R7M00      TopLayer    4.7uH                     1449.000      1330.000      90       Inductor                               
L1         SRR1005-122K            TopLayer    RLB0913-122K              881.000       1503.575      270      Inductor                               
F1         "1A/300V/SLOW BLOW"     TopLayer    0697A1000-05              185.575       800.000       180      Fuse                                   
D2         TY-30UG1D50-150         TopLayer    5mm_LED_2.54mm_pitch      1847.197      139.000       180      "Typical RED GaAs LED"                 
D1         TY-30UR3D50-100         TopLayer    5mm_LED_2.54mm_pitch      2107.197      139.000       180      "Typical RED GaAs LED"                 
CY2        G08F1E222MN0B0S0N0      TopLayer    G08F1E222MN0B0S0N0        1288.693      174.000       180      Capacitor                              
CX1        890334023023CS          TopLayer    890334023023CS            552.000       1583.850      90       Capacitor                              
CR3        TPD1E01B04              TopLayer    DPL2_TEX-M                2552.000      214.000       360      "No Description Available"             
CR2        TPD1E01B04              TopLayer    DPL2_TEX-M                3097.000      234.000       270      "No Description Available"             
CR1        TPD1E01B04              TopLayer    DPL2_TEX-M                3350.000      374.000       360      "No Description Available"             
C4         C1608X7R1E104K080AA     TopLayer    1608[0603]                1482.000      1600.000      270      Capacitor                              
C3         RNU1V151MDN1PH          TopLayer    RR-B_NCH                  1249.000      1758.500      90       Capacitor                              
C2         UTT0J471MPD1TD          TopLayer    CAP_URS_8X9_NCH           1573.898      1043.000      360      Capacitor                              
C1         UCY2W100MPD             TopLayer    CAP_UBX_10X20_NCH         1094.000      1108.425      270      Capacitor                              
Q1         BC817-40                TopLayer    SOT23_N                   1651.000      233.000       270      "NPN General Purpose Amplifier"        
R6         ESR03EZPF1001           TopLayer    J1-0603                   1707.000      394.000       90       Resistor                               
CY1        G08F1E222MN0B0S0N0      TopLayer    G08F1E222MN0B0S0N0        735.307       174.000       180      Capacitor                              
R7         CRGCQ0603J1M0           TopLayer    6-0805_L                  272.000       1578.000      90       Resistor                               
R8         CRGCQ0603J1M0           TopLayer    6-0805_L                  272.000       1446.000      90       Resistor                               
R9         CRGCQ0603J1M0           TopLayer    6-0805_L                  347.000       1578.000      90       Resistor                               
R10        CRGCQ0603J1M0           TopLayer    6-0805_L                  347.000       1445.000      90       Resistor                               
D3         SMBJ5.0A                TopLayer    SMBJSeries_LTF            1599.000      1598.000      270      "Zener Diode"                          
