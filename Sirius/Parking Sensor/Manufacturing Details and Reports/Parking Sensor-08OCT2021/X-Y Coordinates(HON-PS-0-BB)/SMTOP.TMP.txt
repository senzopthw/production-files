*********************************************************************************************************
*                                                                                                       *
* Surface Mount Top Report                                                                              *
*                                                                                                       *
* E:\SENZOPT_ALL\HW_PROJECTS\SIRIUS_PROJECT\PARKING SENSOR\PARKING-SENSOR-V2.13\LAYOUT\HON-PS-0-BB.MAX  *
* Sat Aug 31 13:28:02 2019                                                                              *
*                                                                                                       *
*********************************************************************************************************

 REF DES    VALUE            PACKAGE          FOOTPRINT                      X INSERT   Y INSERT   ROTATION
------------------------------------------------------------------------------------------------------------
61         M HOLE1                           FSHIELD                         1.92 mm    42.71 mm  0       
62                                           FSHIELD                         38.11 mm   5.57 mm   0       
C3         0.1uf            CAP_NP           0603                            17.39 mm   39.85 mm  90      
D1         RST_LED          LED              LED_0603_REV1                   29.26 mm   47.15 mm  90      
R1         1K               R                0603                            29.26 mm   43.78 mm  270     
R2         10K              R                0603                            31.22 mm   43.78 mm  90      
R3         330E             R                0603                            29.05 mm   51.61 mm  0       
R7         100K             R                0603                            21.18 mm   39.09 mm  0       
