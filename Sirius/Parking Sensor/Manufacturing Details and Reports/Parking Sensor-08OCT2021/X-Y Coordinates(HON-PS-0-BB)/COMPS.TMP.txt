*********************************************************************************************************
*                                                                                                       *
* Component List (Generic)                                                                              *
*                                                                                                       *
* E:\SENZOPT_ALL\HW_PROJECTS\SIRIUS_PROJECT\PARKING SENSOR\PARKING-SENSOR-V2.13\LAYOUT\HON-PS-0-BB.MAX  *
* Sat Aug 31 13:28:02 2019                                                                              *
*                                                                                                       *
*********************************************************************************************************

 REF DES    VALUE            PACKAGE          FOOTPRINT                      X LOC      Y LOC      ROTATION
------------------------------------------------------------------------------------------------------------
53         M HOLE1                           MH                              0.61 mm    36.62 mm  0       
54         M HOLE1                           MH                              42.90 mm   36.77 mm  0       
55         M HOLE1                           MH                              43.13 mm   19.15 mm  0       
56         M HOLE1                           MH                              0.61 mm    19.16 mm  0       
57         M HOLE1                           MTHOLE_REV1                     42.18 mm   7.62 mm   0       
58         M HOLE1                           MTHOLE_REV1                     0.15 mm    7.65 mm   0       
59         M HOLE1                           MTHOLE_REV1                     0.19 mm    50.59 mm  0       
60         M HOLE1                           MTHOLE_REV1                     42.24 mm   50.55 mm  0       
61         M HOLE1                           FSHIELD                         1.92 mm    42.71 mm  0       
62                                           FSHIELD                         38.11 mm   5.57 mm   0       
C1         0.1uf            CAP_NP           0603                            5.93 mm    31.71 mm  90      
C2         0.1uf            CAP_NP           0603                            26.15 mm   43.81 mm  270     
C3         0.1uf            CAP_NP           0603                            17.39 mm   39.14 mm  90      
C4         0.1uf            CAP_NP           0603                            15.91 mm   17.56 mm  270     
C5         0.1uf            CAP_NP           0603                            5.93 mm    39.36 mm  270     
C6         0.1uf            CAP_NP           0603                            8.24 mm    39.28 mm  270     
C7         0.1uf            CAP_NP           0603                            15.67 mm   25.16 mm  90      
C8         0.1uF            CAP_NP           0603                            13.79 mm   25.15 mm  90      
C9         18pF             CAP_NP           0603                            14.53 mm   48.77 mm  0       
C10        18pF             CAP_NP           0603                            14.53 mm   43.92 mm  0       
C11        10uF             CAP              B-3528-21                       36.92 mm   20.88 mm  180     
C12        10uF             CAP_NP           B-3528-21                       39.08 mm   34.59 mm  0       
C13        10uF             CAP_NP           0603_REV1                       10.55 mm   25.09 mm  180     
C14        1uF              CAP_NP           0603                            25.26 mm   26.40 mm  90      
C15        1uF              CAP_NP           0603                            28.91 mm   20.87 mm  0       
D1         RST_LED          LED              LED_0603_REV1                   29.26 mm   46.44 mm  90      
D8         RGB              RGB_0            RGB_5MM_REV2                    5.64 mm    45.59 mm  0       
J1         CONN FLEX 12     CONN_FLEX_12     PSY003LD-6P6C-2_REV3            14.24 mm   9.17 mm   0       
J2         ISP              CONN_RCPT_3X2/SM ISP                             35.65 mm   40.40 mm  0       
J3         UART             CONN_FLEX_4      CONN4X1_2.54                    0.72 mm    15.47 mm  0       
M1         ATmega 8A        ATMEGA_8_0       TQFN 32                         9.85 mm    35.32 mm  180     
Q1          NPN TRANSISTOR  BC337_7          BS138_REV1                      32.57 mm   15.85 mm  180     
Q3         PNP BC557        PNP_BCE_0        TO92_REV2                       41.84 mm   10.90 mm  90      
Q4          NPN TRANSISTOR  BC337_5          BS138                           10.38 mm   12.49 mm  270     
Q5         PNP BC557        PNP_BCE_1        TO92_REV1                       8.81 mm    9.75 mm   180     
R1         1K               R                0603                            29.26 mm   44.49 mm  270     
R2         10K              R                0603                            31.22 mm   43.06 mm  90      
R3         330E             R                0603                            28.34 mm   51.61 mm  0       
R5         1K               R                0603                            33.99 mm   17.86 mm  0       
R6         10K              R                0603                            2.08 mm    25.71 mm  180     
R7         100K             R                0603                            20.47 mm   39.09 mm  0       
R8         0E/DNP           R                0603                            6.54 mm    35.54 mm  90      
R9         1K               R                0603                            7.09 mm    12.49 mm  180     
R14        84.5E            R                0603                            9.45 mm    49.00 mm  90      
R15        330E             R                0603                            18.32 mm   6.97 mm   180     
R16        84.5E            R                0603                            7.55 mm    49.01 mm  90      
R17        154E             R                0603                            5.57 mm    49.04 mm  90      
R19        412E             R                0603                            34.94 mm   11.81 mm  0       
R20        10E              R                0603                            26.88 mm   13.88 mm  180     
R21        412E             R                0603                            12.85 mm   19.12 mm  90      
R22        10E              R                0603                            25.86 mm   6.96 mm   180     
R23        DNP              R                0603                            36.97 mm   14.68 mm  270     
R24        DNP              R                0603                            3.05 mm    12.39 mm  180     
SW1        SW               SW_PUSHBUTTON    SW_TCT                          36.40 mm   49.00 mm  180     
U1         PIR              PIR_3            PIR_170_REV2                    21.77 mm   45.61 mm  270     
U2         HC-SR04          US               US_MODULE                       17.93 mm   18.98 mm  0       
U3         LM1117_3.3V      LM1117_0         LM1117                          42.24 mm   24.93 mm  0       
U4         LM324            LM324            SOG.050/14/WG.244/L.325         28.23 mm   22.59 mm  270     
Y1         16MHz            CRYSTAL          XTAL16                          13.11 mm   41.99 mm  90      
