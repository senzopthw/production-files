*********************************************************************************************************
*                                                                                                       *
* CROSS REFERENCE NET LIST REPORT                                                                       *
*                                                                                                       *
* E:\SENZOPT_ALL\HW_PROJECTS\SIRIUS_PROJECT\PARKING SENSOR\PARKING-SENSOR-V2.13\LAYOUT\HON-PS-0-BB.MAX  *
* Sat Aug 31 13:28:02 2019                                                                              *
*                                                                                                       *
*********************************************************************************************************

REF DES    XCOORD       YCOORD       NET NAMES                                
------------------------------------------------------------------------------
53.1        0.61 mm      36.62 mm    << unused >>                                                                       
                                                                                                                        
54.1        42.90 mm     36.77 mm    << unused >>                                                                       
                                                                                                                        
55.1        43.13 mm     19.15 mm    << unused >>                                                                       
                                                                                                                        
56.1        0.61 mm      19.16 mm    << unused >>                                                                       
                                                                                                                        
57.1        42.18 mm     7.62 mm     << unused >>                                                                       
                                                                                                                        
58.1        0.15 mm      7.65 mm     << unused >>                                                                       
                                                                                                                        
59.1        0.19 mm      50.59 mm    << unused >>                                                                       
                                                                                                                        
60.1        42.24 mm     50.55 mm    << unused >>                                                                       
                                                                                                                        
61.1        0.65 mm      43.98 mm    << unused >>                                                                       
                                                                                                                        
62.1        36.84 mm     6.84 mm     << unused >>                                                                       
                                                                                                                        
C1.1        5.93 mm      31.71 mm    RESET                                                                              
C1.2        5.93 mm      30.29 mm    GND                                                                                
                                                                                                                        
C2.1        26.15 mm     43.81 mm    3.3V                                                                               
C2.2        26.15 mm     45.23 mm    GND                                                                                
                                                                                                                        
C3.1        17.39 mm     39.14 mm    PIR_OUT                                                                            
C3.2        17.39 mm     40.56 mm    GND                                                                                
                                                                                                                        
C4.1        15.91 mm     17.56 mm    GND                                                                                
C4.2        15.91 mm     18.98 mm    5V                                                                                 
                                                                                                                        
C5.1        5.93 mm      39.36 mm    5V                                                                                 
C5.2        5.93 mm      40.78 mm    GND                                                                                
                                                                                                                        
C6.1        8.24 mm      39.28 mm    5V                                                                                 
C6.2        8.24 mm      40.71 mm    GND                                                                                
                                                                                                                        
C7.1        15.67 mm     25.16 mm    5V                                                                                 
C7.2        15.67 mm     23.74 mm    GND                                                                                
                                                                                                                        
C8.1        13.79 mm     25.15 mm    N9215944                                                                           
C8.2        13.79 mm     23.73 mm    GND                                                                                
                                                                                                                        
C9.1        14.53 mm     48.77 mm    GND                                                                                
C9.2        13.10 mm     48.77 mm    N9155633                                                                           
                                                                                                                        
C10.1       14.53 mm     43.92 mm    GND                                                                                
C10.2       13.11 mm     43.92 mm    N9155720                                                                           
                                                                                                                        
C11.1       36.92 mm     20.88 mm    5V                                                                                 
C11.2       40.05 mm     20.88 mm    GND                                                                                
                                                                                                                        
C12.1       39.08 mm     34.59 mm    3.3V                                                                               
C12.2       35.95 mm     34.59 mm    GND                                                                                
                                                                                                                        
C13.1       10.55 mm     25.09 mm    PSIA                                                                               
C13.2       11.98 mm     25.09 mm    GND                                                                                
                                                                                                                        
C14.1       25.26 mm     26.40 mm    5V                                                                                 
C14.2       25.26 mm     24.98 mm    GND                                                                                
                                                                                                                        
C15.1       28.91 mm     20.87 mm    PSI                                                                                
C15.2       27.49 mm     20.87 mm    GND                                                                                
                                                                                                                        
D1.1        29.26 mm     46.44 mm    N9127237                                                                           
D1.2        29.26 mm     47.86 mm    RESET                                                                              
                                                                                                                        
D8.1        5.64 mm      45.59 mm    N9253415                                                                           
D8.2        6.91 mm      45.59 mm    GND                                                                                
D8.3        8.18 mm      45.59 mm    N9252055                                                                           
D8.4        9.45 mm      45.59 mm    N9251523                                                                           
                                                                                                                        
J1.1        14.24 mm     9.17 mm     5V                                                                                 
J1.2        15.26 mm     11.71 mm    PIR_OUT                                                                            
J1.3        16.28 mm     9.17 mm     US_OUT                                                                             
J1.4        17.30 mm     11.71 mm    GND                                                                                
J1.5        18.32 mm     9.17 mm     N9258750                                                                           
J1.6        19.34 mm     11.71 mm    PSI                                                                                
J1.7        25.24 mm     9.17 mm     << unused >>                                                                       
J1.8        26.26 mm     11.71 mm    GCON+                                                                              
J1.9        27.28 mm     9.17 mm     RCON-                                                                              
J1.10       28.30 mm     11.71 mm    GCON-                                                                              
J1.11       29.32 mm     9.17 mm     RCON+                                                                              
J1.12       30.24 mm     11.71 mm    << unused >>                                                                       
J1.13       33.28 mm     6.87 mm     << unused >>                                                                       
J1.14       10.28 mm     6.87 mm     << unused >>                                                                       
                                                                                                                        
J2.1        35.65 mm     40.40 mm    MISO                                                                               
J2.2        35.65 mm     42.94 mm    5V                                                                                 
J2.3        38.19 mm     40.40 mm    SCK                                                                                
J2.4        38.19 mm     42.94 mm    MOSI                                                                               
J2.5        40.73 mm     40.40 mm    RESET                                                                              
J2.6        40.73 mm     42.94 mm    GND                                                                                
                                                                                                                        
J3.1        0.72 mm      15.47 mm    5V                                                                                 
J3.2        3.26 mm      15.47 mm    GND                                                                                
J3.3        5.80 mm      15.47 mm    TX                                                                                 
J3.4        8.34 mm      15.47 mm    RX                                                                                 
                                                                                                                        
M1.1        10.55 mm     35.32 mm    N9165211                                                                           
M1.2        11.35 mm     35.32 mm    N9195230                                                                           
M1.3        12.15 mm     35.32 mm    GND                                                                                
M1.4        12.95 mm     35.32 mm    5V                                                                                 
M1.5        13.75 mm     35.32 mm    GND                                                                                
M1.6        14.55 mm     35.32 mm    5V                                                                                 
M1.7        15.35 mm     35.32 mm    N9155633                                                                           
M1.8        16.15 mm     35.32 mm    N9155720                                                                           
M1.9        17.43 mm     34.12 mm    << unused >>                                                                       
M1.10       17.43 mm     33.32 mm    << unused >>                                                                       
M1.11       17.43 mm     32.52 mm    RED                                                                                
M1.12       17.43 mm     31.72 mm    GREEN                                                                              
M1.13       17.43 mm     30.92 mm    G                                                                                  
M1.14       17.43 mm     30.12 mm    B                                                                                  
M1.15       17.43 mm     29.32 mm    MOSI                                                                               
M1.16       17.43 mm     28.52 mm    MISO                                                                               
M1.17       16.15 mm     27.22 mm    SCK                                                                                
M1.18       15.35 mm     27.22 mm    5V                                                                                 
M1.19       14.55 mm     27.22 mm    << unused >>                                                                       
M1.20       13.75 mm     27.22 mm    N9215944                                                                           
M1.21       12.95 mm     27.22 mm    GND                                                                                
M1.22       12.15 mm     27.22 mm    << unused >>                                                                       
M1.23       11.35 mm     27.22 mm    << unused >>                                                                       
M1.24       10.55 mm     27.22 mm    PSIA                                                                               
M1.25       9.30 mm      28.52 mm    US_OUT                                                                             
M1.26       9.30 mm      29.32 mm    << unused >>                                                                       
M1.27       9.30 mm      30.12 mm    << unused >>                                                                       
M1.28       9.30 mm      30.92 mm    << unused >>                                                                       
M1.29       9.30 mm      31.72 mm    RESET                                                                              
M1.30       9.30 mm      32.52 mm    RX                                                                                 
M1.31       9.30 mm      33.32 mm    TX                                                                                 
M1.32       9.30 mm      34.12 mm    N9211712                                                                           
                                                                                                                        
Q1.1        32.57 mm     15.85 mm    N9332215                                                                           
Q1.2        34.47 mm     15.85 mm    GND                                                                                
Q1.3        33.52 mm     13.65 mm    N9332261                                                                           
                                                                                                                        
Q3.1        41.84 mm     10.90 mm    GCON+                                                                              
Q3.2        39.24 mm     13.50 mm    N9332233                                                                           
Q3.3        41.84 mm     16.10 mm    5V                                                                                 
                                                                                                                        
Q4.1        10.38 mm     12.49 mm    N9332367                                                                           
Q4.2        10.38 mm     14.39 mm    GND                                                                                
Q4.3        12.58 mm     13.44 mm    N9332409                                                                           
                                                                                                                        
Q5.1        8.81 mm      9.75 mm     RCON+                                                                              
Q5.2        6.21 mm      7.15 mm     N9332141                                                                           
Q5.3        3.61 mm      9.75 mm     5V                                                                                 
                                                                                                                        
R1.1        29.26 mm     44.49 mm    N9127237                                                                           
R1.2        29.26 mm     43.07 mm    5V                                                                                 
                                                                                                                        
R2.1        31.22 mm     43.06 mm    5V                                                                                 
R2.2        31.22 mm     44.49 mm    RESET                                                                              
                                                                                                                        
R3.1        28.34 mm     51.61 mm    RESET                                                                              
R3.2        29.76 mm     51.61 mm    N9134072                                                                           
                                                                                                                        
R5.1        33.99 mm     17.86 mm    GREEN                                                                              
R5.2        32.57 mm     17.86 mm    N9332215                                                                           
                                                                                                                        
R6.1        2.08 mm      25.71 mm    N9165211                                                                           
R6.2        3.51 mm      25.71 mm    GND                                                                                
                                                                                                                        
R7.1        20.47 mm     39.09 mm    PIR_OUT                                                                            
R7.2        21.89 mm     39.09 mm    GND                                                                                
                                                                                                                        
R8.1        6.54 mm      35.54 mm    PIR_OUT                                                                            
R8.2        6.54 mm      34.12 mm    N9211712                                                                           
                                                                                                                        
R9.1        7.09 mm      12.49 mm    RED                                                                                
R9.2        8.52 mm      12.49 mm    N9332367                                                                           
                                                                                                                        
R14.1       9.45 mm      49.00 mm    B                                                                                  
R14.2       9.45 mm      47.57 mm    N9251523                                                                           
                                                                                                                        
R15.1       18.32 mm     6.97 mm     N9258750                                                                           
R15.2       19.75 mm     6.97 mm     GND                                                                                
                                                                                                                        
R16.1       7.55 mm      49.01 mm    G                                                                                  
R16.2       7.55 mm      47.59 mm    N9252055                                                                           
                                                                                                                        
R17.1       5.57 mm      49.04 mm    MOSI                                                                               
R17.2       5.57 mm      47.62 mm    N9253415                                                                           
                                                                                                                        
R19.1       34.94 mm     11.81 mm    N9332233                                                                           
R19.2       33.52 mm     11.81 mm    N9332261                                                                           
                                                                                                                        
R20.1       26.88 mm     13.88 mm    GND                                                                                
R20.2       28.30 mm     13.88 mm    GCON-                                                                              
                                                                                                                        
R21.1       12.85 mm     19.12 mm    N9332141                                                                           
R21.2       12.85 mm     17.70 mm    N9332409                                                                           
                                                                                                                        
R22.1       25.86 mm     6.96 mm     GND                                                                                
R22.2       27.28 mm     6.96 mm     RCON-                                                                              
                                                                                                                        
R23.1       36.97 mm     14.68 mm    N9332233                                                                           
R23.2       36.97 mm     16.10 mm    5V                                                                                 
                                                                                                                        
R24.1       3.05 mm      12.39 mm    5V                                                                                 
R24.2       4.47 mm      12.39 mm    N9332141                                                                           
                                                                                                                        
SW1.1       36.40 mm     49.00 mm    N9134072                                                                           
SW1.2       31.40 mm     49.00 mm    GND                                                                                
                                                                                                                        
U1.1        19.97 mm     47.41 mm    GND                                                                                
U1.2        23.57 mm     47.41 mm    PIR_OUT                                                                            
U1.3        23.57 mm     43.81 mm    3.3V                                                                               
                                                                                                                        
U2.1        17.93 mm     18.98 mm    5V                                                                                 
U2.2        20.47 mm     18.98 mm    N9195230                                                                           
U2.3        23.01 mm     18.98 mm    N9165211                                                                           
U2.4        25.55 mm     18.98 mm    GND                                                                                
                                                                                                                        
U3.1        42.24 mm     24.93 mm    GND                                                                                
U3.2        39.94 mm     24.93 mm    3.3V                                                                               
U3.3        37.64 mm     24.93 mm    5V                                                                                 
U3.4        39.97 mm     30.94 mm    3.3V                                                                               
                                                                                                                        
U4.1        28.23 mm     22.59 mm    PSIA                                                                               
U4.2        28.23 mm     23.86 mm    PSIA                                                                               
U4.3        28.23 mm     25.13 mm    PSI                                                                                
U4.4        28.23 mm     26.40 mm    5V                                                                                 
U4.5        28.23 mm     27.67 mm    << unused >>                                                                       
U4.6        28.23 mm     28.94 mm    << unused >>                                                                       
U4.7        28.23 mm     30.21 mm    << unused >>                                                                       
U4.8        33.21 mm     30.21 mm    << unused >>                                                                       
U4.9        33.21 mm     28.94 mm    << unused >>                                                                       
U4.10       33.21 mm     27.67 mm    << unused >>                                                                       
U4.11       33.21 mm     26.40 mm    GND                                                                                
U4.12       33.21 mm     25.13 mm    << unused >>                                                                       
U4.13       33.21 mm     23.86 mm    << unused >>                                                                       
U4.14       33.21 mm     22.59 mm    << unused >>                                                                       
                                                                                                                        
Y1.1        13.11 mm     41.99 mm    N9155720                                                                           
Y1.2        13.11 mm     46.87 mm    N9155633                                                                           
                                                                                                                        
