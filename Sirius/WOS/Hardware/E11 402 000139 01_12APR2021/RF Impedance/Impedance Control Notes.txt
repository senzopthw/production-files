Author: Hamara Chaudhuri
        Systems Engineer, Graylinx Pvt Ltd, Bangalore
        Date of release: 12 April 2021

Fabrication Drawing for PCB No: E11 402 000139 01

Impedance Control:

a) Areas marked as Blue-called out as 50 ohms Impedance Track on Fabrication Drawing, are 50ohm Microstrip.
Impedance Control 50 ohms at 2440 MHz. Vendor shall provide Test coupon on PCB Array and Test Report using a Time-Domain Reflectometer (TDR).

The nominal Dimension of 50 ohms microstrip traces are:

-> 0.2086 mm wide Transmission Line on Layer 1
-> RF Ground Plane on Layer 2.
-> 0.127 mm thick prepreg insulator between Layer 1 & Layer 2 with dielectric constant of approximately 4.6.

Meeting the 50 ohm tested impedance is the primary requirement.
These nominal dimensions and material properties can be adjusted within reason by the vendor to meet a tested impedance of 50 Ohms +/- 10% on the test coupon.

b) Areas marked as Red-called out as 100 ohms Impedance Track on Fabrication Drawing, are 100 ohm Differential Microstrip.
Impedance Control 100 ohms Differential at 2440 MHz. Vendor shall provide Test coupon on PCB Array and Test Report using a Time-Domain Reflectometer (TDR).

The nominal Dimension of 100 ohms Differential microstrip traces are:

-> Two 0.1633 mm wide Transmission Line (Differential Pair) separated by an 0.25 mm gap on Layer 1 (0.4133 mm center to center)
-> RF Ground Plane on Layer 2.
-> 0.127 mm thick prepreg insulator between Layer 1 & Layer 2 with dielectric constant of approximately 4.6.

Meeting the 100 ohm tested impedance is the primary requirement.
These nominal dimensions and material properties can be adjusted within reason by the vendor to meet a tested impedance of 100 Ohms Differential +/- 10% on the test coupon.

